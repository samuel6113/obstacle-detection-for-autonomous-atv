#ifndef COLLISION_CHECK_NODE_HPP
#define COLLISION_CHECK_NODE_HPP

// ROS
#include <sensor_msgs/PointCloud2.h>
#include <nav_msgs/OccupancyGrid.h>
// related classes
#include "collision_check/ptCloud_processing.hpp"
#include "collision_check/ptCloud_mapping.hpp"


class CollisionCheckNode
{
public:
    CollisionCheckNode(const ros::NodeHandle &nh, const ros::NodeHandle &pnh);
    ~CollisionCheckNode();

private:

    ros::NodeHandle             nh_;
    ros::Subscriber             cloud_sub_;
    ros::Publisher              map_pub_;

    PtCloudProcessing           ptCloud_processor_;
    PtCloudMapping              ptCloud_mapping_;

    float                       param_ransac_threshold_;
    float                       param_height_threshold_;
    float                       param_map_resolution_;
    int                         param_map_x_;
    int                         param_map_y_;
    int                         param_obstacle_size_;


    nav_msgs::OccupancyGrid     output_map_;

    void CloudCB(const sensor_msgs::PointCloud2::ConstPtr &msg);

    bool isPowerOfTwo(const int x)
    {
        int base = 1;
        while ( base < x && base < 2147483648 ) // 2^31
            base = (base << 1);
        return (base == x);
    };
    
};

#endif