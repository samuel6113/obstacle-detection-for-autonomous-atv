#ifndef PTCLOUD_MAPPING_HPP
#define PTCLOUD_MAPPING_HPP

// ROS
#include <sensor_msgs/PointCloud2.h>
#include <nav_msgs/OccupancyGrid.h>
// PCL
#include <pcl/point_cloud.h>
#include <pcl_conversions/pcl_conversions.h>
// Cpp
#include <vector>


class PtCloudMapping
{
public:
    PtCloudMapping();
    ~PtCloudMapping();

    // TODO: mapping only support pcl::pointcloud2 
    //      but tranfrompointcloud doesnt support it
    //      so we change it all to sensor_msgs::ptCloud2 and change it back
    nav_msgs::OccupancyGrid GenerateMap(const sensor_msgs::PointCloud2 &cloud);

    void SetMapY(const int &param_y) { param_y_ = param_y; };

    void SetMapX(const int &param_x) { param_x_ = param_x; };

    void SetMapResolution(const float &param_res) { param_resolution_ = param_res; };

    void SetObstacleSize(const int &size) { obstacle_size_ = size; };

private:

    /********************/
    /* member variables */
    /********************/
    pcl::PCLPointCloud2             input_cloud_;

    nav_msgs::OccupancyGrid         output_map_;
    
    // X: forward, Y: left, Z: up
    // map dims are fixed to power of 2
    // collision check requires high computation performance, 
    // use bit operation instead of multiplication
    float                           param_resolution_;      // [m/cell]
    int                             param_y_;               //fixed to power of 2
    int                             param_x_;               //fixed to power of 2
    int                             param_grid_w_ = static_cast<int>(param_y_ / param_resolution_);
    int                             param_grid_h_ = static_cast<int>(param_x_ / param_resolution_);
    float                           lowBoundX_;
    float                           lowBoundY_ = -(param_y_>>1);    //-y/2 
    unsigned int                    seq_;


    // TODO: explain & param
    int                             obstacle_size_;
    int                             obstacle_bound_ = (obstacle_size_ << 1) + 1;
    int                             array_size_ = (obstacle_bound_ * obstacle_bound_) << 1;  


    /********************/
    /* member functions */
    /********************/

    // put helper function inside first, move it out later

    void InitMap();

    void ResetMap();

    std::vector<int> PtCoordToGridCoord(float coordX, float coordY);

    std::vector<int> NodeIDToGridCoord(int NodeID);

    int GridCoordToNodeID(int gridX, int gridY);

    int PtCoordToNodeID(float coordX, float coordY);

    std::vector<int> GetNeighbour(int gridX, int gridY);


};


#endif