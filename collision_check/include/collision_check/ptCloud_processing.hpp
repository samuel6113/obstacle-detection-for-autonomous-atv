#ifndef PTCLOUD_PROCESSING_HPP
#define PTCLOUD_PROCESSING_HPP

// ROS
#include <sensor_msgs/PointCloud2.h>
// PCL
#include <pcl/point_cloud.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/sample_consensus/ransac.h>
#include <pcl/sample_consensus/sac_model_plane.h>
#include <pcl/filters/extract_indices.h>
#include "pcl_ros/transforms.h"
// TF
#include <tf/transform_listener.h>

typedef pcl::PointCloud<pcl::PointXYZ>::Ptr     pcl_cloud_ptr;
typedef pcl::PointIndices::Ptr                  pcl_idx_ptr;

class PtCloudProcessing
{
public:
    PtCloudProcessing();
    ~PtCloudProcessing();

    void UpdateTF(const tf::StampedTransform &transform);    // maybe use bool output

    void SetRansacThreshold(const float &threshold) { param_ransac_threshold_ = threshold; };

    void SetHeightThreshold(const float &threshold) { param_height_threshold_ = threshold; };

    sensor_msgs::PointCloud2 
    Process(const pcl::PCLPointCloud2 &inputCloud);

private:

    /********************/
    /* member variables */
    /********************/
    pcl::PCLPointCloud2                 filtered_PCL_cloud_;
    sensor_msgs::PointCloud2            transformed_cloud_;

    std::vector<int>                    inliers_;
    std::vector<int>                    lower_indices_;

    pcl_cloud_ptr                       cloud_;
    pcl_cloud_ptr                       ransac_cloud_;
    pcl_cloud_ptr                       height_cloud_;

    pcl_idx_ptr                         ransac_inliers_;
    pcl_idx_ptr                         height_inliers_;

    tf::StampedTransform                transform_;

    // user input variables
    float                               param_ransac_threshold_;
    float                               param_height_threshold_;

    /********************/
    /* member functions */
    /********************/
    void Ransac(const pcl::PCLPointCloud2 &inputCloud);

    void HeightFilter();

    void RansacOutliers();

    void PtCloudTransform();
   
};


#endif 