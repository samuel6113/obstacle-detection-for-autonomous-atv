#include "collision_check/ptCloud_processing.hpp"


PtCloudProcessing::PtCloudProcessing() : 
param_ransac_threshold_(0.8), param_height_threshold_(-1.3), 
cloud_(new pcl::PointCloud<pcl::PointXYZ>), 
ransac_cloud_(new pcl::PointCloud<pcl::PointXYZ>),
height_cloud_(new pcl::PointCloud<pcl::PointXYZ>),
ransac_inliers_(new pcl::PointIndices()),
height_inliers_(new pcl::PointIndices())
{


}

PtCloudProcessing::~PtCloudProcessing()
{
    //TODO
    return;
}


void PtCloudProcessing::Ransac(const pcl::PCLPointCloud2 &inputCloud)
{
    //from pcl::PCLPointCloud2 to pcl::PointCloud
    //cloud is input data here
    pcl::fromPCLPointCloud2(inputCloud, *cloud_);

    // created RandomSampleConsensus object and compute the appropriated model
    // use cloud into model_plane
    pcl::SampleConsensusModelPlane<pcl::PointXYZ>::Ptr
        model_plane (new pcl::SampleConsensusModelPlane<pcl::PointXYZ>(cloud_));

    pcl::RandomSampleConsensus<pcl::PointXYZ> ransac (model_plane);
    ransac.setDistanceThreshold(param_ransac_threshold_);
    ransac.computeModel();
    //get the inliers index in vector
    ransac.getInliers(inliers_);
}


void PtCloudProcessing::HeightFilter()
{
    // check every point's height, get their idx depending on height
    for (unsigned int i = 0; i < ransac_cloud_->points.size(); ++i)
    {
        if (ransac_cloud_->points[i].z < param_height_threshold_)
        {
            lower_indices_.push_back(i);
        }
    }

    pcl::ExtractIndices<pcl::PointXYZ> extract;
    // Extract the inliers

    height_inliers_->indices = lower_indices_;   

    extract.setInputCloud(ransac_cloud_);
    extract.setIndices(height_inliers_);
    extract.setNegative(true);
    extract.filter(*height_cloud_);

    pcl::toPCLPointCloud2(*height_cloud_, filtered_PCL_cloud_);

    lower_indices_.clear();
}


void PtCloudProcessing::RansacOutliers()
{
    pcl::ExtractIndices<pcl::PointXYZ> extract;
    // Extract the inliers

    ransac_inliers_->indices = inliers_;

    extract.setInputCloud(cloud_);
    extract.setIndices(ransac_inliers_);
    extract.setNegative(true);
    extract.filter(*ransac_cloud_);
}


void PtCloudProcessing::PtCloudTransform()
{
    sensor_msgs::PointCloud2 ros_type_cloud;

    pcl_conversions::moveFromPCL(filtered_PCL_cloud_, ros_type_cloud);

    pcl_ros::transformPointCloud(std::string("localWorld"), transform_, ros_type_cloud, transformed_cloud_);
}


void PtCloudProcessing::UpdateTF(const tf::StampedTransform &transform)
{
    transform_ = transform;
}


sensor_msgs::PointCloud2 
PtCloudProcessing::Process(const pcl::PCLPointCloud2 &inputCloud)
{
    // RANSAC with plane model
    Ransac(inputCloud);

    // First filter: get outliers from RANSAC
    RansacOutliers();

    // Second filter: get rid of anything below a certain user-specified threshould in sensor frame
    HeightFilter();

    // Transform the point cloud from sensor frame to world frame
    PtCloudTransform();

    return transformed_cloud_;

}
