#include <ros/ros.h>
#include "collision_check/collision_check_node.hpp"

CollisionCheckNode::CollisionCheckNode(const ros::NodeHandle &nh, const ros::NodeHandle &pnh) : 
    nh_(pnh), 
    param_ransac_threshold_(0.8),
    param_height_threshold_(-1.3),
    param_map_x_(256),
    param_map_y_(64),
    param_map_resolution_(1.0),
    param_obstacle_size_(2)
{
    cloud_sub_  = nh_.subscribe("target_points", 1, &CollisionCheckNode::CloudCB, this); 

    map_pub_    = nh_.advertise<nav_msgs::OccupancyGrid>("RAtest/ptCloud_map", 1);

    if (nh_.getParam("ransac_threshold", param_ransac_threshold_))
    {
        ptCloud_processor_.SetRansacThreshold(param_ransac_threshold_);
        ROS_INFO("RANSAC threshold set to %f", param_ransac_threshold_);
    }

    if (nh_.getParam("height_threshold", param_height_threshold_))
    {
        ptCloud_processor_.SetHeightThreshold(param_height_threshold_);
        ROS_INFO("Height threshold set to %f", param_height_threshold_);
    }

    if (nh_.getParam("map_x", param_map_x_))
    {
        if (isPowerOfTwo(param_map_x_))
            ptCloud_mapping_.SetMapX(param_map_x_);
        else 
            ROS_WARN("Map dim should be power of 2 for bit manipulation");
        ROS_INFO("Map length in X set to %d", param_map_x_);
    }

    if (nh_.getParam("map_y", param_map_y_))
    {
        if (isPowerOfTwo(param_map_y_))
            ptCloud_mapping_.SetMapY(param_map_y_);
        else 
            ROS_WARN("Map dim should be power of 2 for bit manipulation");
        ROS_INFO("Map length in Y set to %d", param_map_y_);
    }

    if (nh_.getParam("map_resolution", param_map_resolution_))
    {
        ptCloud_mapping_.SetMapResolution(param_map_resolution_);
        ROS_INFO("Map resolution set to %f", param_map_resolution_);
    }

    if (nh_.getParam("obstacle_size", param_obstacle_size_))
    {
        ptCloud_mapping_.SetObstacleSize(param_obstacle_size_);
        ROS_INFO("Obstacle dilation size set to %d", param_obstacle_size_);
    }
}


CollisionCheckNode::~CollisionCheckNode()
{
    return;
}


void CollisionCheckNode::CloudCB(const sensor_msgs::PointCloud2::ConstPtr &msg)
{
    // update transform from current tf tree  
    tf::StampedTransform    transform;
    tf::TransformListener   listener;   
    
    try
    {
        listener.lookupTransform("localWorld", "velodyne", ros::Time::now(), transform);
    }
    catch (tf::TransformException &ex) 
    {
        ROS_ERROR("%s",ex.what());
    }

    ptCloud_processor_.UpdateTF(transform);


    /* turn ptCloud into map */ 
    pcl::PCLPointCloud2         cb_cloud;
    sensor_msgs::PointCloud2    ros_cloud = *msg;

    // TODO: this is weird, I can't just pass *msg to the function
    pcl_conversions::moveToPCL(ros_cloud, cb_cloud);

    sensor_msgs::PointCloud2    processed_cloud;

    processed_cloud = ptCloud_processor_.Process(cb_cloud);

    output_map_ = ptCloud_mapping_.GenerateMap(processed_cloud);

    map_pub_.publish(output_map_);
}



int main (int argc, char** argv)
{
    ros::init(argc, argv, "collision_check_node");
    ros::NodeHandle nh;
    ros::NodeHandle pnh("~");

    CollisionCheckNode CollisionCheckNode(nh, pnh);

    ros::spin();

    return 0;
}