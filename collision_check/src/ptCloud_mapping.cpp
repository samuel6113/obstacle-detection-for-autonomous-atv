#include "collision_check/ptCloud_mapping.hpp"


PtCloudMapping::PtCloudMapping() :
param_y_(64), param_x_(256), param_resolution_(1), lowBoundX_(0), obstacle_size_(2)
{
    InitMap();
}


PtCloudMapping::~PtCloudMapping()
{
    return;
}


nav_msgs::OccupancyGrid 
PtCloudMapping::GenerateMap(const sensor_msgs::PointCloud2 &cloud)
{
    // TODO: why cant I just pass the input arg to the function?
    sensor_msgs::PointCloud2 ros_cloud;
    ros_cloud = cloud;
    pcl_conversions::moveToPCL(ros_cloud, input_cloud_);

    pcl::PointCloud<pcl::PointXYZ>::Ptr localCloud (new pcl::PointCloud<pcl::PointXYZ>);

    pcl::fromPCLPointCloud2(input_cloud_, *localCloud);


    int NodeID, boundaryID;
    std::vector<int> gridCoord, obstacle_boundary;
    for (int i = 0; i < localCloud->points.size(); ++i)
    {
    
        NodeID = PtCoordToNodeID(localCloud->points[i].y, -localCloud->points[i].x); //FIXME
        //assert(NodeID >= 0);
        //assert(NodeID < grid_H * grid_W);  

        gridCoord = NodeIDToGridCoord(NodeID);

        obstacle_boundary = GetNeighbour(gridCoord[0],gridCoord[1]);

        for (int j = 0; j < array_size_; j+=2)
        {
            boundaryID = GridCoordToNodeID(obstacle_boundary[j],obstacle_boundary[j+1]);

            //assert(boundaryID >= 0);
            //assert(boundaryID < grid_H * grid_W);
            //assert(output_map_.data[boundaryID] >= 0);
            //assert(output_map_.data[boundaryID] <= 100);
              
            int8_t data = output_map_.data[boundaryID];
            /* check if data is correct */
            int8_t temp = data;
            if ( temp > 100 && temp < 0) 
                printf("\33[33m[Warning] Invalid map data, out of bound \33[0m \n");
              
            output_map_.data[boundaryID] = (data >= 100) ? 100 : data + 10;
        }
    }

    return output_map_;
}


void PtCloudMapping::InitMap()
{
    geometry_msgs::Pose ORIGIN;
    //ORIGIN.position.y = param_y_/2;
    //ORIGIN.position.x = lowBoundY_
    //tf::Quaternion q;
    //q.setRPY(0,0,-PI/2);
    //tf::quaternionTFToMsg(q,ORIGIN.orientation);
    /*
    ORIGIN.orientation.x = 0.70710;
    ORIGIN.orientation.y = -0.70710;
    ORIGIN.orientation.z = 0;
    ORIGIN.orientation.w = 0;
    */
    output_map_.info.origin     = ORIGIN;
    output_map_.info.resolution = param_resolution_;
    output_map_.info.width      = param_grid_w_;
    output_map_.info.height     = param_grid_h_;
    output_map_.header.stamp    = ros::Time::now();
    output_map_.header.seq      = seq_++;
    output_map_.header.frame_id = std::string("localWorld");

    ResetMap();
}


void PtCloudMapping::ResetMap()
{
    output_map_.data.resize(param_grid_w_ * param_grid_h_);
    
    for ( unsigned int yidx = 0; yidx < param_grid_w_; ++yidx ){
      for ( unsigned int xidx = 0; xidx < param_grid_h_; ++xidx ){
          
          int idx = yidx + (xidx << 6) ; //* grid_W; //WARNING

          output_map_.data[idx] = 0;
        }
    }   
}


std::vector<int> PtCloudMapping::PtCoordToGridCoord(float coordX, float coordY)
{
    std::vector<int> gridCoord;
    float percentX, percentY;

    percentX = (coordX - lowBoundX_) / param_x_;
    percentY = -(coordY + lowBoundY_) / param_y_;

    // map do not cover the wholde cloud
    // prevent getting coordinate out of bound
    if (percentX >= 1.0){ percentX = 0.99; }
    if (percentY >= 1.0){ percentY = 0.99; }
    if (percentX <= 0.0){ percentX = 0.0; }
    if (percentY <= 0.0){ percentY = 0.0; }

    gridCoord.push_back(static_cast<int>(percentX*param_grid_h_));
    gridCoord.push_back(static_cast<int>(percentY*param_grid_w_));

    //assert(gridCoord[0] >= 0);
    //assert(gridCoord[0] < grid_H * grid_W); 
    //assert(gridCoord[1] >= 0);
    //assert(gridCoord[1] < grid_H * grid_W); 

    return gridCoord;
}


std::vector<int> PtCloudMapping::NodeIDToGridCoord(int NodeID)
{
    std::vector<int> gridCoord;

    int temp = NodeID & (param_grid_h_ - 1 );  
    gridCoord.push_back((NodeID- temp) / param_grid_w_);
    gridCoord.push_back(temp); //NodeID % grid_H; 

    return gridCoord;
}


int PtCloudMapping::GridCoordToNodeID(int gridX, int gridY)
{
    int NodeID;

    NodeID = gridY + (gridX << 6) ; //* grid_W; //WARNING

    return NodeID;
}


int PtCloudMapping::PtCoordToNodeID(float coordX, float coordY)
{
    int                 NodeID;
    std::vector<int>    gridCoord; 

    gridCoord = PtCoordToGridCoord(coordX, coordY);

    NodeID = GridCoordToNodeID(gridCoord[0], gridCoord[1]);

    return NodeID;
}


std::vector<int> PtCloudMapping::GetNeighbour(int gridX, int gridY)
{
    std::vector<int> gridCoordXY;
    int count = 0;

    for ( unsigned int i = 0; i < obstacle_bound_; ++i )
    {
      for ( unsigned int j = 0; j < obstacle_bound_; ++j )
      {
        gridCoordXY[count] = (gridX-obstacle_size_) + i;
        gridCoordXY[count+1] = (gridY-obstacle_size_) + j;

        count += 2;
      }
    }

    return gridCoordXY;     
}

