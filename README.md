# Viking Collision Check Module #

* This repo turns point cloud data into a 2D occupancy grid map.
* It subscribes to odometry topic and tf tree for point cloud frame transformation. 
* It's the collision check module of an autonomous all-terrain vehicle. 
* RANSAC Segmentation function from PCL is used to detect obstacles.

# Environment #

* Ubuntu 14.04
* ROS Indigo
* Point Cloud Library

![collision_check.jpg](https://bitbucket.org/repo/yBd58r/images/2342445339-collision_check.jpg)